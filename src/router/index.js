import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Login from '@/components/Login';
import SignUp from '@/components/SignUp';
import Cart from '@/components/Cart';
import LotteryCompany from '@/components/lotteryCompany/LotteryCompany';
import NationalLottery from '@/components/NationalLottery';
import ResultsAndAwards from '@/components/resultsAndAwards/ResultsAndAwards';
import Awards from '@/components/Awards';
import LotteryCompanyCode from '@/components/lotteryCompany/LotteryCompanyCode';
import Checkout from '@/components/checkout/checkout';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: SignUp,
    },
    {
      path: '/lottery-company',
      name: 'lottery-company',
      component: LotteryCompany,
    },
    {
      path: '/national-lottery',
      name: 'national-lottery',
      component: NationalLottery,
    },
    {
      path: '/results-and-awards',
      name: 'results-and-awards',
      component: ResultsAndAwards,
    },
    {
      path: '/awards',
      name: 'awards',
      component: Awards,
    },
    {
      path: '/lottery-company/code',
      name: 'lottery-company-code',
      component: LotteryCompanyCode,
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart,
    },
    {
      path: '/checkout',
      name: 'checkout',
      component: Checkout,
    },
  ],
  mode: 'history',
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});
